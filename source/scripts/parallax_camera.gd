extends Camera2D

var vel = 150

func _ready():
	set_process(true)
	pass

func _process(delta):
	set_offset(get_offset() + Vector2(0, -1) * vel * delta)
	pass
